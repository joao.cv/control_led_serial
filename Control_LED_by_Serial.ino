/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Fade
*/

int led = 9;           // the PWM pin the LED is attached to
int brightness = 0;    // how bright the LED is
int pbrightness = 0;   // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by
int s = 0;
int count = 0;

// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // set the brightness of pin 9:

  if (Serial.available() > 0) {
    count++;
    s = Serial.read() - '0';
    if (s == -38){
      count = 0;
      brightness = pbrightness;
    }
    else{    
      if (count == 1){
        pbrightness  = 0;
      }
      pbrightness = 10*pbrightness  + s;    
    }
    Serial.print(pbrightness);
    Serial.print("\n");  
  }
  analogWrite(led, brightness);

  delay(30);
}
